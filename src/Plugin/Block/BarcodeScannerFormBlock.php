<?php

namespace Drupal\barcode_scanner\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;



/**
 * Provides a barcode_scanner form Block.
 *
 * @Block(
 *   id = "barcode_scanner_form_block",
 *   admin_label = @Translation("Barcode Scanner block"),
 *   category = @Translation("Barcode Scanner"),
 * )
 */
class BarcodeScannerFormBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {
      $form = \Drupal::formBuilder()->getForm('Drupal\barcode_scanner\Form\BarcodeScannerLookupForm');
      return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration() {
        return [
            'barcode_scanner_block_name' => $this->t('Barcode Lookup'),
        ];
    }

}
