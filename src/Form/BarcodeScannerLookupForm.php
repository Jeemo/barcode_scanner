<?php

namespace Drupal\barcode_scanner\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\barcode_scanner\BarcodeFinder;

/**
 * Barcode Scanner Lookup Form.
 */
class BarcodeScannerLookupForm extends FormBase {

    private $config;

    /**
     * @inheritdoc
     */
    public function __construct() {
        $this->config = $this->configFactory()->getEditable('barcode_scanner.settings');
    }

    /**
     * @inheritdoc
     */
    public function getFormId() {
        return 'barcode_scanner_lookup_form';
    }

    /**
     * @inheritdoc
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $form['barcode'] = [
            '#type' => 'number',
            '#title' => $this->t('Barcode'),
            '#description' => $this->t('Enter the barcode you wish to look up.'),
            '#maxlength' => 64,
            '#size' => 64,
            '#weight' => '0',
            '#min' => 1,
            '#step' => 1,
        ];

      $form['submit_add'] = [
        '#type' => 'submit',
        '#title' => $this->t('Add'),
        '#description' => $this->t(
          'Submit the barcode you wish to add to stock.'
        ),
        '#value' => $this->t('Add'),
      ];

      $form['submit_subtract'] = [
        '#type' => 'submit',
        '#title' => $this->t('Subtract'),
        '#description' => $this->t(
          'Submit the barcode you wish to subtract from stock.'
        ),
        '#value' => $this->t('Subtract'),
      ];

      $form['submit_edit'] = [
        '#type' => 'submit',
        '#title' => $this->t('Edit'),
        '#description' => $this->t(
          'Edit the item.'
        ),
        '#value' => $this->t('Edit'),
      ];

        return $form;
    }

    /**
     * @inheritdoc
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $finder = new BarcodeFinder();
        $url = $finder->get($form_state->getValue('barcode'), $form_state->getValue('op'));
        return $form_state->setRedirectUrl($url);
    }

}
