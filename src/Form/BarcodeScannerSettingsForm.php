<?php

namespace Drupal\barcode_scanner\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 */
class BarcodeScannerSettingsForm extends ConfigFormBase {

  /**
   * @var $config
   */
  private $config;

  /**
   * @inheritdoc
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
    $this->config = $config_factory->getEditable('barcode_scanner.settings');
  }

  /**
   * @inheritdoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('config.factory')
      );
  }

  /**
   * @inheritdoc
   */
  protected function getEditableConfigNames() {
    return [
      'barcode_scanner.settings',
    ];
  }

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return 'barcode_scanner_settings';
  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $output = '';
    $output .= '<h3>' . t('About') . '</h3>';
    $output .= '<p>' . t('Barcode Scanner integrated with barcodelookup.com.') . '</p>';
    $output .= '<p>' . t('The primary barcode scanner form page is located at `/scan`.  I like to set this as the front page of the site.') . '</p>';
    $output .= '<p>' . t('There is a also a custom block ("Barcode Scanner block") for the form if you prefer that.') . '</p>';
    $output .= '<p>' . t('This is written to work with the Barcode Lookup API as of Dec 10, 2023.') . '</p>';

    $form['barcodelookup_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Barcode Lookup API Key'),
      '#description' => $this->t('This requires a paid subscription to barcodelookup.com.'),
      '#default_value' => $this->config->get('barcodelookup_api_key'),
      '#prefix' => $output,
    ];
    $default_value = $this->config->get('barcodelookup_api_url');
    if (empty($default_value)) {
      $default_value = 'https://api.barcodelookup.com/v3/products?barcode=%barcode&formatted=y&key=%api_key';
    }
    $form['barcodelookup_api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Barcode Lookup API URL'),
      '#description' => $this->t('<p>The latest URL can be retrieved from this external link: <a href="https://www.barcodelookup.com/api" target="_blank">Barcode Lookup API Documentation</a>. <br />Please replace the key value in the url with the token "%api_key" and make sure to include barcode value in the query params with the token "%barcode".'),
      '#default_value' => $default_value,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * @inheritdoc
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $url = $form_state->getValue('barcodelookup_api_key');
    if ($url !== '') {
      if (substr_count($form_state->getValue('barcodelookup_api_url'), '%barcode') !== 1) {
        $form_state->setErrorByName('barcodelookup_api_url', $this->t("Token '%barcode' should be used once."));
      }
      if (substr_count($form_state->getValue('barcodelookup_api_url'), '%api_key') !== 1) {
        $form_state->setErrorByName('barcodelookup_api_url', $this->t("Token '%api_key' should be used once."));
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('barcode_scanner.settings')
      ->set('barcodelookup_api_key', $form_state->getValue('barcodelookup_api_key'))
      ->set('barcodelookup_api_url', $form_state->getValue('barcodelookup_api_url'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
